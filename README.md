# lynis-bridge

Accept lynis-report.dat files (upload), transform it into json and put it into a mariadb to visualize the result with grafana.

![lynis_grafana](lynis_grafana.png)

## docker deployment

| env | default value |
| --- | --- |
| `DATABASE_HOST` | `mariadb` |
| `DATABASE_USER` | `lynis` |
| `DATABASE_PASSWORD ` | `lynis` |
| `DATABASE` | `lynis` |
| `SSL` | _value does not matter, if set, https is enabled_ |

When the env variable `SSL` is set, the container looks for `/tmp/key.pem` and `/tmp/key.cert`.  
When they are not found, it will generate a self signed certificate on the fly.

Gunicorn is started with 5 worker processes.

## client notes

When using lynis-cron, you can post the result to your lynis-bridge with curl (_yes, the user-agent must be set to `lynis-bridge`, otherwise the lynis-bridge will response http code 403_).

```
curl -A "lynis-bridge" -F data=@lynis-report.dat http://<lynis-bridge>:8080/upload
```

# database

Currently only Mariadb >= 10.3 is supported.  
The table `reports` is using `WITH SYSTEM VERSIONING`. So you got a report history about your hosts.  
You just need to query them ;)

When `/opt/mariadb.pem` is given, `lynis-bridge` will use encrypted in transit using the Transport Layer Security (TLS) protocol to the mariadb host.


# credits.

`lynis-report-converter.pl` is taken from https://github.com/d4t4king/lynis-report-converter

# SCM

| **host** | **category** |
| --- | --- |
| https://git.osuv.de/m/lynis-bridge | origin |
| https://gitlab.com/markuman/lynis-bridge | pull mirror |
| https://github.com/markuman/lynis-bridge | push mirror |
